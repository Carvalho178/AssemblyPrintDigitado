org 100h

name "exibirString"

exibe_cab:
    mov dx, offset cab
    mov ah, 9
    int 21h

inicio:
    xor cx, cx
    mov di, cx
    mov dx, offset digite
    mov ah, 9
    int 21h 
    
principal:
    mov ah, 0
    int 16h
    
    cmp al, 27
    je sair
    
    mov [di], al
    inc di
    inc cx   
    
    mov ah, 0eh
    int 10h
    
    cmp al, 13
    je print_digitado 
    
    jmp principal
    

print_digitado:
    sub di, cx    
    mov dx, offset enter
    mov ah, 9
    inc di
    dec cx
    jnz print_char
    
    jmp inicio  
   
cab db "Enter para exibir o que foi digitado, ESC - Sair", 0Dh, 0Ah, 24h
enter db  0Dh, 0Ah,"Voce digitou: ", 0Dh, 0Ah, 24h
esc db  0Dh, 0Ah,"Adeus!", 0Dh, 0Ah, 24h
digite db  0Dh, 0Ah,"Digite: ", 0Dh, 0Ah, 24h    

sair:
    mov dx, offset esc
    mov ah, 9
    int 21h
                                         
    int 21h
    

print_char:

    mov al, [di]
    mov ah, 0eh
    int 10h
